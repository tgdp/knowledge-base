# Knowledge base

[![License: MIT-0](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/license/mit-0) [![CHAOSS DEI: Bronze](https://gitlab.com/tgdp/governance/-/raw/main/CHAOSS-Bronze-Badge-small.png?ref_type=heads)](https://badging.chaoss.community/)

The Knowledge Base repository is a space for collaborating with the Knowledge Base working group for [The Good Docs Project](https://www.thegooddocsproject.dev/).

The goal of the Knowledge Base working group is to improve the internal documentation at The Good Docs Project so that our community members can find the content that will help them succeed in the project.
They regularly work with project leaders and teams to document the concepts, processes, and changes in the project with the goal of helping improve communication transparency and accessibility to our project members.

Join this group if you are interested in learning more about knowledge management, internal documentation best practices, and community management.
